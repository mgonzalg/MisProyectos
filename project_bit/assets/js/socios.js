// Declaración e inicialización
var dni = null;

function enviar() {
	dni = $('#dni').val();

	if (dni.length <= 6) {
		alert('Error, no puede ser menor que 6');
	} else if (dni.length >= 10) {
		alert('Error, no puede ser mayor que 10');
	} else {
		$('#dni').parents('form').submit(); // Submit de formulario
	}
}

